import PushNotification from 'react-native-push-notification';

const LOCAL_NOTIFICATION_SCHEDULE_ID = '830397';

export const localNotificationSchedule = (
  title: undefined | string,
  message: string,
  milliseconds: number,
  ongoing = false
) => {
  PushNotification.localNotificationSchedule({
    id: LOCAL_NOTIFICATION_SCHEDULE_ID,
    title,
    message,
    ongoing,
    date: new Date(Date.now() + milliseconds),
  });
};

export const cancelLocalNotificationSchedule = () => {
  PushNotification.cancelLocalNotifications({
    id: LOCAL_NOTIFICATION_SCHEDULE_ID,
  });
};
