import DeviceInfo from 'react-native-device-info';

const getVersion = () => {
  return DeviceInfo.getVersion();
};

export default getVersion;
