import moment from 'moment';
import React, { useState } from 'react';
import { Text } from 'react-native';
import BackgroundTimer from 'react-native-background-timer';
import { Button } from 'react-native-paper';
import styled from 'styled-components/native';
import {
  cancelLocalNotificationSchedule,
  localNotificationSchedule,
} from '../../utils/Notification';
import getVersion from '../../utils/Version';

const getMilliseconds = (startTime: string, milliseconds: number): number => {
  const end = moment(startTime).add({ milliseconds });
  const now = moment();

  return end.diff(now);
};

enum STATUS {
  WORK_READY,
  WORK_PAUSE,
  WORK_RUNNING,
  BREAK_READY,
  BREAK_RUNNING,
}

enum MILLISECONDS {
  WORK = 1500000,
  BREAK = 300000,
}

enum MESSAGES {
  WORK_FINISH = 'You have cleared a Pomodoro!',
  BREAK_FINISH = 'Your break has ended!',
}

const TimerScreen = () => {
  const [time, setTime] = useState<number>(MILLISECONDS.WORK);
  const [timerId, setTimerId] = useState<undefined | number>(undefined);
  const [status, setStatus] = useState<STATUS>(STATUS.WORK_READY);
  const countdown = moment.utc(time).format('mm:ss');
  const version = getVersion();

  const onStart = (
    milliseconds: number,
    startStatus: STATUS,
    endStatus: STATUS,
    endTime: MILLISECONDS
  ) => {
    const startTime = moment().format();
    const id = BackgroundTimer.setInterval(() => {
      const ms = getMilliseconds(startTime, milliseconds);

      if (ms > 0) {
        setStatus(startStatus);
        setTime(ms);
        setTimerId(id);
        return;
      }

      onEnd(id, endStatus, endTime);
    }, 1000);
  };

  const onEnd = (id: number, endStatus: STATUS, endTime: MILLISECONDS) => {
    BackgroundTimer.clearInterval(id);
    setStatus(endStatus);
    setTime(endTime);
  };

  const onStartWork = () => {
    setTime(MILLISECONDS.WORK - 1000);

    localNotificationSchedule(
      undefined,
      MESSAGES.WORK_FINISH,
      MILLISECONDS.WORK
    );
    onStart(
      MILLISECONDS.WORK,
      STATUS.WORK_RUNNING,
      STATUS.BREAK_READY,
      MILLISECONDS.BREAK
    );
  };

  const onPauseWork = (id?: number) => {
    if (id) {
      cancelLocalNotificationSchedule();
      BackgroundTimer.clearInterval(id);
      setStatus(STATUS.WORK_PAUSE);
    }
  };

  const onContinueWork = (timeMs: number) => {
    setTime(timeMs - 1000);

    localNotificationSchedule(undefined, MESSAGES.WORK_FINISH, timeMs);
    onStart(
      timeMs,
      STATUS.WORK_RUNNING,
      STATUS.BREAK_READY,
      MILLISECONDS.BREAK
    );
  };

  const onStopWork = (id?: number) => {
    if (id) {
      cancelLocalNotificationSchedule();
      BackgroundTimer.clearInterval(id);
      setStatus(STATUS.WORK_READY);
      setTime(MILLISECONDS.WORK);
    }
  };

  const onStartBreak = () => {
    setTime(MILLISECONDS.BREAK - 1000);

    localNotificationSchedule(
      undefined,
      MESSAGES.BREAK_FINISH,
      MILLISECONDS.BREAK
    );
    onStart(
      MILLISECONDS.BREAK,
      STATUS.BREAK_RUNNING,
      STATUS.WORK_READY,
      MILLISECONDS.WORK
    );
  };

  return (
    <Screen>
      <Body>
        <Timer>{countdown}</Timer>

        {status === STATUS.WORK_READY && (
          <PrimaryButton
            onPress={onStartWork}
            uppercase={false}
            mode="contained"
          >
            Start
          </PrimaryButton>
        )}

        {status === STATUS.WORK_RUNNING && (
          <PrimaryButton
            onPress={() => onPauseWork(timerId)}
            uppercase={false}
            mode="contained"
          >
            Pause
          </PrimaryButton>
        )}

        {status === STATUS.WORK_PAUSE && (
          <>
            <PrimaryButton
              onPress={() => onContinueWork(time)}
              uppercase={false}
              mode="outlined"
            >
              Continue
            </PrimaryButton>

            <PrimaryButton
              onPress={() => onStopWork(timerId)}
              uppercase={false}
              mode="outlined"
            >
              Stop
            </PrimaryButton>
          </>
        )}

        {status === STATUS.BREAK_READY && (
          <PrimaryButton
            onPress={onStartBreak}
            uppercase={false}
            mode="contained"
          >
            Start
          </PrimaryButton>
        )}

        {status === STATUS.BREAK_RUNNING && (
          <PrimaryButton
            onPress={() => onStopWork(timerId)}
            uppercase={false}
            mode="outlined"
          >
            End
          </PrimaryButton>
        )}
      </Body>
      <Footer>
        <Text>{version}</Text>
      </Footer>
    </Screen>
  );
};

const Screen = styled.SafeAreaView`
  flex: 1;
`;

const Timer = styled.Text`
  font-size: 64;
`;

const PrimaryButton = styled(Button)`
  margin-top: 20;
`;

const Body = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;
const Footer = styled.View`
  justify-content: center;
  align-items: center;
`;

export default TimerScreen;
