import React from 'react';
import renderer from 'react-test-renderer';
import TimerScreen from '.';

describe('GIVEN I am "user type" "any"', () => {
  describe('WHEN I am at "Timer Screen"', () => {
    const tree = renderer.create(<TimerScreen />).toJSON();

    it('THEN I Should see "default UI"', () => {
      expect(tree).toMatchSnapshot();
    });
  });
});
