import React from 'react';
import TimerScreen from './src/screens/TimerScreen';

const App = () => {
  return <TimerScreen />;
};

export default App;
