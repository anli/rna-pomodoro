## [1.0.1](https://gitlab.com/anli/rna-pomodoro/compare/1.0.0...1.0.1) (2019-10-20)

### Bug Fixes

- **timer screen:** fix onContinueWork to set notifcation with break time ([acd9be9](https://gitlab.com/anli/rna-pomodoro/commit/acd9be9493a1c89643b39ea3a210a466e67c6f69)), closes [#23](https://gitlab.com/anli/rna-pomodoro/issues/23)

### Features

- **timer screen:** add messages for WORK_FINISH and BREAK_FINISH ([7137d8c](https://gitlab.com/anli/rna-pomodoro/commit/7137d8c3a54a510dfc5f6ebd19f87b6b0b72f225)), closes [#24](https://gitlab.com/anli/rna-pomodoro/issues/24)

# 1.0.0 (2019-10-15)

### Features

- **package, jest.setup, timer screen:** add background-timer lib ([9b64acb](https://gitlab.com/anli/rna-pomodoro/commit/9b64acbd68b373b91cb1cfb53b3c09ba1ecee5be)), closes [#12](https://gitlab.com/anli/rna-pomodoro/issues/12)
- **package, tests, timer screen:** add version UI ([ad2bdfc](https://gitlab.com/anli/rna-pomodoro/commit/ad2bdfcb1e406901a8a3fac1f67916cf47470363)), closes [#15](https://gitlab.com/anli/rna-pomodoro/issues/15)
- **package.json, timerscreen:** add moment library, start timer method ([85bc4e1](https://gitlab.com/anli/rna-pomodoro/commit/85bc4e13a73b44d61ffdca2cae0ca4a6c010aee5)), closes [#1](https://gitlab.com/anli/rna-pomodoro/issues/1)
- **test, notification, package, timer screen:** add local notification ([4349c6a](https://gitlab.com/anli/rna-pomodoro/commit/4349c6afa3e11e19130196ca7a66927c092447ac)), closes [#7](https://gitlab.com/anli/rna-pomodoro/issues/7)
- **timerscreen:** add on continue work method ([f811817](https://gitlab.com/anli/rna-pomodoro/commit/f811817a8a39d1674a0146e14e356d9d0f68ea9a)), closes [#3](https://gitlab.com/anli/rna-pomodoro/issues/3)
- **timerscreen:** add onContinueWork with params, break running UI ([1355c5b](https://gitlab.com/anli/rna-pomodoro/commit/1355c5b0531a091c0405f4b9846bcc51c5e2cf84)), closes [#11](https://gitlab.com/anli/rna-pomodoro/issues/11)
- **timerscreen:** add onEnd, onStartBreak, change STATUS ([3ff0b7c](https://gitlab.com/anli/rna-pomodoro/commit/3ff0b7cbfd93a5113f827d82522aac0ac1dede13)), closes [#6](https://gitlab.com/anli/rna-pomodoro/issues/6) [#8](https://gitlab.com/anli/rna-pomodoro/issues/8)
- **timerscreen:** add onStopWork method ([ac7abe0](https://gitlab.com/anli/rna-pomodoro/commit/ac7abe065490a40af9a95cdbd24ba8429b6e65b6)), closes [#4](https://gitlab.com/anli/rna-pomodoro/issues/4)
- **timerscreen:** add pause work button and function, change to ms ([233dfb0](https://gitlab.com/anli/rna-pomodoro/commit/233dfb0d54dad588966dc65dcb48f7c703389f3e)), closes [#2](https://gitlab.com/anli/rna-pomodoro/issues/2)
- **timerscreen:** add timer screen, rn paper, styled components ([20e1c5e](https://gitlab.com/anli/rna-pomodoro/commit/20e1c5edc0a5da74a2a4adcfdfc67ef3c166ab22)), closes [#5](https://gitlab.com/anli/rna-pomodoro/issues/5)
