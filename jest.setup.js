import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

jest.mock('react-native-push-notification', () => {
  return {
    localNotificationSchedule: jest.fn(),
    cancelLocalNotifications: jest.fn(),
  };
});

jest.mock('react-native-background-timer', () => {
  return {
    setInterval: jest.fn(),
    clearInterval: jest.fn(),
  };
});

jest.mock('react-native-device-info', () => {
  return {
    getVersion: jest.fn(),
  };
});
