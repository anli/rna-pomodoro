# Pomodoro

Pomodoro is a React Native App build to track your time based on the Pomodoro principle.

[![Build Status](https://app.bitrise.io/app/7eb5b94b4c75d76f/status.svg?token=tcLrjHficRzo6mX7A52qRw&branch=develop)](https://app.bitrise.io/app/7eb5b94b4c75d76f)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Setup React Native

### Installing

```
git clone https://gitlab.com/anli/rna-pomodoro.git
npm ci
npm run android
```

## Running the tests

```
npm run test
```

### And coding style tests

```
npm run lint
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/anli/rna-pomodoro/-/tags).

## Authors

- **Su Anli** - _Initial work_ - [@anli](https://gitlab.com/anli)
